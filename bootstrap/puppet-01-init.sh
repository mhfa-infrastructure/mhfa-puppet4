#!/usr/bin/env bash

set -e

# Remove default hiera 3 file.
puppet resource file /etc/puppetlabs/puppet/hiera.yaml ensure=absent

# Link puppetcode from production environment.
puppet resource file /etc/puppetlabs/code/environments/production ensure=link target=/vagrant/puppetcode force=true

# Link puppetcode to be accessed from vagrant home folder.
puppet resource file /home/vagrant/puppetcode ensure=link target=/vagrant/puppetcode

# Puppet-lint.
puppet resource package epel-release ensure=installed && puppet resource package rubygem-puppet-lint ensure=installed
