#!/usr/bin/env bash

set -e

# Shortcut to this script so it can be re-run on demand.
puppet resource file /home/vagrant/puppet-02-apply.sh ensure=link target=/vagrant/bootstrap/puppet-02-apply.sh > /dev/null

# Provision with puppet.
if [ -e /etc/puppetlabs/code/environments/production/manifests/site.pp ]
then
  echo 'Apply production/manifests/site.pp'
  /usr/bin/sudo /opt/puppetlabs/puppet/bin/puppet apply /etc/puppetlabs/code/environments/production/manifests/site.pp
else
  echo 'No production/manifests/site.pp file!'
fi
