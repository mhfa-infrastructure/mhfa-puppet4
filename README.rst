Getting started
---------------

Created directory for cache::

    mkdir -p ~/.vagrant-mhfa-yum-cache

Start the box::

    vagrant up

Login::

    vagrant ssh

To test MySQL, grab the password from ``mysql::server::root_password``, then
inside the box::

    mysql -uroot -p
