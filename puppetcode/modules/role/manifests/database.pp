# database role written for staging db node for now.
class role::database {
  class { '::profile::base': } ->
  class { '::profile::mysql': } ->
  class { '::profile::mysql::db': }

  class { '::profile::network': }
}
