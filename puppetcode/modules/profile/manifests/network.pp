# cofigures firewalld zone for MHFA public facing nodes.
#
# this profile will place each internal interface from the trusted key from
# hiera and place them into the trusted firewalld zone.
class profile::network {
  # skips if we are running vagrant, this only applies on the public cloud.
  if ($::virtual != 'virtualbox') {
    $interfaces = lookup('network::interface::trusted') |$key| { {} }

    $interfaces.each |$interface| {
      # updating ifcfg-${interface} twice as ifdown scripts undo it, but it is
      # required to restart the nic. see:
      # https://bugzilla.redhat.com/show_bug.cgi?id=1381314
      augeas { "${interface}_ZONE":
        context => "/files/etc/sysconfig/network-scripts/ifcfg-${interface}",
        changes => 'set ZONE trusted',
      }~>
      exec { "profile__network_stop_${interface}":
        command     => "ifdown ${interface}",
        path        => '/usr/sbin',
        refreshonly => true,
      }->
      augeas { "${interface}_ZONE_again":
        context => "/files/etc/sysconfig/network-scripts/ifcfg-${interface}",
        changes => 'set ZONE trusted',
      }~>
      exec { "profile__network_start_${interface}":
        command     => "ifup ${interface}",
        path        => '/usr/sbin',
        refreshonly => true,
      }
    }
  }
}
