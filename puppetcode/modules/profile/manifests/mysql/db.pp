# create a database for each value in mysql::server::db hash.
class profile::mysql::db {
  $databases = lookup('mysql::server::db') |$key| { {} }

  $databases.each |$dbname, $password| {
    ::mysql::db { $dbname:
      ensure   => present,
      user     => $dbname,
      password => $password,
      host     => '%',
      grant    => 'ALL',
      charset  => 'utf8',
      collate  => 'utf8_general_ci',
    }
  }
}
