# base os stuff are common across all nodes.
class profile::base {
  $latest = [
    'vim',
    'tree',
  ]

  package { 'epel-release':
    ensure => installed,
  }

  package { $latest:
    ensure => latest,
  }

  file { '/etc/localtime':
    ensure => link,
    target => '/usr/share/zoneinfo/Australia/Melbourne',
  }

  # mhfa general administrative acccount.
  user { 'mhfa':
    ensure         => present,
    password       => '*',
    gid            => 'mhfa',
    groups         => [ 'wheel'],
    home           => '/home/mhfa',
    managehome     => true,
    purge_ssh_keys => true,
    require        => [
      Group['mhfa'],
    ],
  }

  group { 'mhfa':
    ensure => present,
  }

  ssh_authorized_key { 'sun@shonentai':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/sunny-mhfa-notebook.pub.txt')),
  }

  ssh_authorized_key { 'cass@raccoon':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/cass.pub.txt')),
  }

  ssh_authorized_key { 'rene':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/rene.pub.txt')),
  }

  ssh_authorized_key { 'triyuga@gmail.com':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/tim.pub.txt')),
  }

  ssh_authorized_key { 'sunny@mhfa-notebook2':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/sunny-notebook2.pub.txt')),
  }

  ssh_authorized_key { 'sunny@mhfa-phone':
    user => 'mhfa',
    type => 'ssh-rsa',
    key  => chomp(file('profile/ssh/sunny-mhfa-phone.pub.txt')),
  }

  file { '/etc/sudoers.d/mhfa-sudoers':
    source => 'puppet:///modules/profile/base/mhfa-sudoers',
    owner  => 'root',
    group  => 'root',
    mode   => 'ug=r,o=',
  }
}
