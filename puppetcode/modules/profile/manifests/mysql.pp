# MHFA mysql profile. we are practically running mariadb on rhel/centos 7 at
# this point.
class profile::mysql {
  $root_password = lookup('mysql::server::root_password') |$key| {
    'MHFA default database password 1.'
  }
  $max_allowed_packet = lookup('mysql::server::max_allowed_packet') |$key| {
    '128M'
  }

  class { '::mysql::server':
    root_password           => $root_password,
    remove_default_accounts => true,
    restart                 => true,
    override_options        => {
      'mysqld' => {
        'max_allowed_packet' => $max_allowed_packet,
        'bind-address'       => '::',
      }
    },
  }

  File {
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }

  # acquia default mysql config from dev desktop.
  file { '/etc/my.cnf.d/acquia.cnf':
    ensure => present,
    source => 'puppet:///modules/profile/mysql/acquia.cnf',
    notify => Service['mariadb'],
  }

  # adding required systemd config for mariadb open_files_limit to work.
  # @see https://ma.ttias.be/increase-open-files-limit-in-mariadb-on-centos-7-with-systemd/
  file { '/etc/systemd/system/mariadb.service.d':
    ensure => directory,
  }->
  file { '/etc/systemd/system/mariadb.service.d/limits.conf':
    ensure => present,
    source => 'puppet:///modules/profile/mysql/limits.conf',
  }~>
  exec { '/usr/bin/systemctl daemon-reload':
    refreshonly => true,
    notify      => Service['mariadb'],
  }
}
